# Privacy

Privacy is an ever growing concern in an age where everything and everyone is interconnected.
The following are some references that I have found useful and/or educational.
I've also included some tools that I find useful, or promising.

## Resources

- [PEPR 2023 - A People-First Approach to Introducing Process and Tools for Technical Privacy Review](https://www.youtube.com/watch?v=o3puBs9K2FA) - Ryan Tenorio, Brex
- [PEPR 2023 - Designing a Private Logging Pipeline](https://www.usenix.org/conference/pepr23/presentation/mukherjee) - Mekhola Mukherjee and Thomas Vannet, Google
- [7 Principles of privacy by design](https://en.wikipedia.org/wiki/Privacy_by_design?useskin=vector#Foundational_principles)
- [DELF: Safeguarding deletion correctness in Online Social Networks](https://research.facebook.com/publications/delf-safeguarding-deletion-correctness-in-online-social-networks/)
    - Covers the framework that guarantees that data will be deleted as requested by users. For example, when a user account is deleted, all data belonging to a user should be removed.

## Tools

- [Ethyca](https://ethyca.com/)
    - Provides data mapping, data classification, and privacy requests.
- [Privado](https://www.privado.ai/)
    - Provides data mapping and data flow analysis.
- [uBlock Origin](https://github.com/gorhill/uBlock#ublock-origin)
    - An ad blocker. Ads track you around the internet, so blocking them is great :smile:
- [ClearURLs](https://docs.clearurls.xyz/1.26.1/)
    - Removes tracking information from URLs. Unique identifiers in URLs also provide a trail of your online activity. This extension tries to remove these identifiers.
