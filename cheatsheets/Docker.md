# Docker Cheatsheet

The [Dockerfile reference](https://docs.docker.com/reference/dockerfile/#) is a resource that I
frequent whenever I want to optimize an image build. I highly recommend reading it eveyr so often
since it continuously gets updated.

## Use latest Dockerfile syntax

You can enable newer Dockerfile features by using this special directive at the top of the file.

```Dockerfile
# syntax=docker/dockerfile:1
```

This pulls in the latest v1 Dockerfile syntax support which includes things like [here-documents](https://docs.docker.com/reference/dockerfile/#here-documents).

## [Here-Documents](https://docs.docker.com/reference/dockerfile/#here-documents)

This features lets you use a here-document style string in your `RUN` _and_ `COPY` commands:

Before

```Dockerfile
RUN \
set -ex && \
apt-get update && \
apt-get install -y vim
```

After

```Dockerfile
RUN <<EOT
set -ex
apt-get update
apt-get install -y vim
EOT
```

## [RUN](https://docs.docker.com/reference/dockerfile/#run)

### `--mount`

I almost always include this in my Dockerfiles to help speed up `go build`, `apt-get`, and any other package manager.
To effectively use this, make sure you run these in a separate builder stage whenever possible.

```Dockerfile
# Go
RUN --mount=type=cache,target=/root/.cache/go-build \
  go build ...
```

```Dockerfile
# Apt
RUN rm -f /etc/apt/apt.conf.d/docker-clean; echo 'Binary::apt::APT::Keep-Downloaded-Packages "true";' > /etc/apt/apt.conf.d/keep-cache
RUN --mount=type=cache,target=/var/cache/apt,sharing=locked \
  --mount=type=cache,target=/var/lib/apt,sharing=locked \
  apt update && apt-get --no-install-recommends install -y gcc
```

```Dockerfile
# DNF and derivatives
RUN --mount=type=cache,target=/var/cache/dnf,sharing=locked \
    microdnf install gcc
```

### `--network`

Access to a network is something that I think should _not_ be given. This is often considered to be over vigilant, but I appreciate the extra security.

```Dockerfile
# Ensure that you only used vendored dependencies when running pip install.
RUN --network=none pip install --find-links wheels mypackage
```

## [ADD](https://docs.docker.com/reference/dockerfile/#add)

I use the `ADD` command frequently in builder stages to download things without the need of `wget` or `cURL`.

```Dockerfile
FROM docker.io/library/busybox:1 AS builder

ADD --checksum=sha256:aab8e15785c997ae20f9c88422ee35d962c4562212bb0f879d052a35c8307c7f https://go.dev/dl/go1.22.1.linux-amd64.tar.gz /
```

If you specify a tar file (as in the above example) then it's autoextracted into `dst`. If you want _specific_ files from the archive,
then you can combine this with the `--exclude` option as well.

## Good practices

This section contains tips that harden the resulting image, and helps reduce the chance of an attacking actor from escaping the container.
It's important to note that [containers are not a security boundary](https://cloud.google.com/blog/products/gcp/exploring-container-security-an-overview)!
So, it should be assumed that the same security guidelines apply as if the container was any other OS installation.

### Use a non-root user

Always run the image as a non-root user, and use a distroless base image to further remove dependencies that can be used as [gadget](https://en.wikipedia.org/wiki/Return-oriented_programming?useskin=vector) in an exploit.

```Dockerfile
RUN addgroup --gid 1001 gitlab && adduser -S gitlab -G gitlab
USER gitlab
```

### Set shell options

These options help uncover bugs in shell scripts.

- `-o pipefail`: If supported by the shell, like in Bash, then this ensures that a piped command exits successfully (exit code 0).
- `-e`: Exit immediately if a command fails in a pipeline.
- `-u`: Ensure that all variables are set.

```Dockerfile
SHELL ["/bin/bash", "-euo", "pipefail"]
```

### Use the exec form for `ENTRYPOINT`, `SHELL`, and `CMD` commands

If you don't use the exec form for `ENTRYPOINT` commands then the entrypoint specified _does not_ get PID 1 assigned.
Since PID 1 receives the `SIGINT` and `SIGTERM` signals, you'll get a container that does not exit as intended, and therfore cannot gracefully terminate.
For `SHELL` and `CMD` commands, the exec form prevents argument munging.

### Tooling

The [hadolint](https://github.com/hadolint/hadolint) tool is a great way to automate these and many other good practices.

```yaml
docker-hadolint:
  image: docker.io/library/hadolint/hadolint:latest-debian
  script:
    - mkdir -p reports
    - hadolint -f gitlab_codeclimate Dockerfile > reports/hadolint-$(md5sum Dockerfile | cut -d" " -f1).json
  artifacts:
    name: "$CI_JOB_NAME artifacts from $CI_PROJECT_NAME on $CI_COMMIT_REF_SLUG"
    expire_in: 1 day
    when: always
    reports:
      codequality:
        - "reports/*"
    paths:
      - "reports/*"
```

## Resources

- [Development guidelines](https://docs.docker.com/develop/dev-best-practices/)
- [General guidelines](https://docs.docker.com/develop/develop-images/guidelines/)
- [Security guidlines](https://docs.docker.com/develop/security-best-practices/)
