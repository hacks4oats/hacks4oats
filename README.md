# About Me :wave:

## Location

Washington, D.C. (UTC-4|UTC-5) :city_sunrise:

## Summary

Hi, I'm Oscar and it's a delight to meet you! I'm a backend engineer on the [Secure: Composition Analysis team](https://about.gitlab.com/handbook/engineering/development/sec/secure/composition-analysis/). I have experience in the Security, Privacy and Software engineering fields. If you would like for me to review your Go code or have any questions related to Composition Analysis, I would be happy to do so.

## How I work

My work schedule is non-linear on some days, so I prefer to work asynchronously and appreciate `@` comments since I defer to my todo list first triaging my tasks. I recognize that sometimes it's a lot easier (or required) to discuss issues synchronously, so I welcome synchronous communication as well. If you'd like to meet and talk through something in a synchronous manner, ping me through Slack or drop me an invite to a Zoom meeting. :smile:

## Communication

I am passionate about the work that I do, and frequently use emojis and exclamation points to communicate this. That being said, I understand that this is not to everyone's preference, so if this is something that you would prefer I not do when communicating with you, please let me know, and I will adjust my communication style. Similarly, if there's anything else I can do to create an environment where you feel safe, included and with a sense of belonging, please point this out to me so I can make the necessary adjustments.

If you need to communicate with me through Slack, please include context on how I can assist you best in the _initial_ message.
This helps me reply to you quickly with an answer. For more context, see [no-hello](https://nohello.net/en/).

### Reviews

If I'm at capacity and there are no other reviewers available, you may assign your MR for me to review if I'm a maintainer or reviewer for the project. Similarly, you may also request for me to review your MR if I have previously reviewed it and have the context of the changes. I make use of [conventional comments](https://conventionalcomments.org/) when reviewing code to reduce the ambiguity of my intent and tone. Likewise, I appreciate when the same is done if you are reviewing my code. 🤝

## Social Profile

### Tech Interests

- Distributed Systems :computer:
- Security :shield:
- Privacy :closed_lock_with_key:
- Programming Languages :floppy_disk:

### Non-Tech Interests

- Board Games :game_die:
- Backpacking :mountain:
- Biking :bicyclist:
- Video Games :video_game:
- Reading :book:
- Football/Soccer :soccer:
