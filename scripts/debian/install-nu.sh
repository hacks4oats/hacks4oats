#!/usr/bin/bash

set -euo pipefail

nu_version="0.93.0"
nu_os="linux-gnu"
nu_arch="x86_64"
download_url="https://github.com/nushell/nushell/releases/download/${nu_version}/nu-${nu_version}-${nu_arch}-${nu_os}-full.tar.gz"

wget -qO- "$download_url" | tar --strip-components=1 -xzv -C /usr/local/bin

mkdir -p ~/.config/nushell
cp ./scripts/nushell/*.nu ~/.config/nushell/
