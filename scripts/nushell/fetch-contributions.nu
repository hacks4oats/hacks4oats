[
  anchore/grype
  anchore/syft
  aquasecurity/trivy
] | each { |$repo|
  let output = ($repo | str snake-case | append "json" | str join '.');
  gh pr ls --repo $repo --limit 100 --state all --author="@me" --json=id,createdAt,mergedAt,closedAt,author,title,url |
  from json |
  update author { |it| $it.author.login } |
  save -f $output;
}
